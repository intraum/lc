#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=C:\Program Files (x86)\AutoIt3\Icons\au3.ico
#AutoIt3Wrapper_Compile_Both=y
#AutoIt3Wrapper_Res_ProductName=Сравнение списков
#AutoIt3Wrapper_Res_Language=1049
#AutoIt3Wrapper_AU3Check_Parameters=-d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         https://gitlab.com/intraum

сравнение двух списков

выделение ктрл+а в текстовых полях:	https://www.autoitscript.com/forum/topic/154066-ctrl-a-select-all-expected-keys-produce-ding-sound/?do=findComment&comment=1111920


#ce ----------------------------------------------------------------------------

#include <Array.au3>
; для сравнения массивов
#include "zavisimosti\ArraysCompare.au3"
#include <File.au3>
; для кодов для MsgBox
#include <MsgBoxConstants.au3>
#include <Date.au3>
; для выделения содержимого полей
#include <GuiEdit.au3>
; -- Created with ISN Form Studio 2 for ISN AutoIt Studio -- ;
#include <StaticConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <GuiButton.au3>
#include <EditConstants.au3>
#include <ProgressConstants.au3>


#Region ; инициализация

; используемый метод управлением пользовательскими интерфейсами
Opt("GUIOnEventMode", 1)

Global $g_sWinTitle = "Сравнение списков", _
		$g_sConfigFilePath = @ScriptDir & "\" & "config.ini", _
		$g_sConfigSection = "general", $g_sKeyWSmode = "ws"
Global $g_iWSmode
Global $g_sResUnique1, $g_sResUnique2, $g_sResMathes, $g_sResCurrent
Global $g_oGUImain, $g_oGUIres

init()

Func init()

	; для установки радиобатона по пробелам в последнее используемое положение
	$g_iWSmode = Int(IniRead($g_sConfigFilePath, $g_sConfigSection, $g_sKeyWSmode, ""))
	Local $iWSall, $iWSnoTrailing, $iWSOnlyInner
	Switch $g_iWSmode
		Case 3
			$iWSall = 0
			$iWSnoTrailing = 0
			$iWSOnlyInner = 1
		Case 2
			$iWSall = 0
			$iWSnoTrailing = 1
			$iWSOnlyInner = 0
		Case Else
			$iWSall = 1
			$iWSnoTrailing = 0
			$iWSOnlyInner = 0
	EndSwitch

	GUImainInit($iWSall, $iWSnoTrailing, $iWSOnlyInner)
	GUIresInit()
EndFunc   ;==>init


Func GUImainInit($iWSall, $iWSnoTrailing, $iWSOnlyInner)
	Local $hGUI
	Local $hBtnStart
	Local $hFieldList1
	Local $hFieldList2
	Local $hBtnUnique1
	Local $hBtnMatches
	Local $hBtnUnique2
	Local $hRadioWSall
	Local $hRadioWSnoTrailing
	Local $hRadioWSOnlyInner
	Local $hBtnClear1
	Local $hBtnClear2
	Local $hDummySelectAll
	Local $hProgbar

	$hGUI = GUICreate($g_sWinTitle, 642, 498, -1, -1, -1, -1)
	GUISetOnEvent($GUI_EVENT_CLOSE, "_Exit", $hGUI)
	$hBtnStart = GUICtrlCreateButton("Сравнить", 425, 397, 110, 48, -1, -1)
	GUICtrlSetOnEvent(-1, "startMain")
	GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
	$hFieldList1 = GUICtrlCreateEdit("", 47, 45, 270, 173, -1, -1)
	; убираем ограничение на количество символов в текстовом поле
	GUICtrlSendMsg(-1, $EM_LIMITTEXT, -1, 0)
	$hFieldList2 = GUICtrlCreateEdit("", 344, 45, 251, 173, -1, -1)
	; убираем ограничение на количество символов в текстовом поле
	GUICtrlSendMsg(-1, $EM_LIMITTEXT, -1, 0)
	GUICtrlCreateGroup("", 21, 7, 600, 235, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "0xF0F0F0")
	GUICtrlCreateLabel("Список 1", 39, 21, 61, 15, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "-2")
	GUICtrlCreateLabel("Список 2", 364, 21, 61, 15, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "-2")
	$hBtnUnique1 = GUICtrlCreateButton("Уникальное из 1", 47, 286, 127, 39, -1, -1)
	GUICtrlSetOnEvent(-1, "GUIresUnique1Show")
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $GUI_DISABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Уникальные элементы из списка 1")
	$hBtnMatches = GUICtrlCreateButton("Общее", 274, 286, 115, 40, -1, -1)
	GUICtrlSetOnEvent(-1, "GUIresMatchesShow")
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $GUI_DISABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Общие элементы для обоих списков")
	$hBtnUnique2 = GUICtrlCreateButton("Уникальное из 2", 467, 287, 128, 39, -1, -1)
	GUICtrlSetOnEvent(-1, "GUIresUnique2Show")
	GUICtrlSetState(-1, BitOR($GUI_SHOW, $GUI_DISABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Уникальные элементы из списка 2")
	GUICtrlCreateGroup("Обработка пробелов", 21, 362, 296, 120, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "0xF0F0F0")
	GUICtrlCreateGroup("Результаты", 21, 258, 600, 86, -1, -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetBkColor(-1, "0xF0F0F0")
	$hRadioWSall = GUICtrlCreateRadio("Учитывать все", 47, 388, 127, 20, -1, -1)
	GUICtrlSetState(-1, BitOR($GUI_CHECKED, $GUI_SHOW, $GUI_ENABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Сравнение как есть")
	ControlCommand($hGUI, "", $hRadioWSall, $iWSall = 1 ? "Check" : "UnCheck", "")
	$hRadioWSnoTrailing = GUICtrlCreateRadio("Игнор закрывающих", 47, 416, 153, 20, -1, -1)
	GUICtrlSetState(-1, BitOR($GUI_UNCHECKED, $GUI_SHOW, $GUI_ENABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Не учитывать пробельные символы в конце строк")
	ControlCommand($hGUI, "", $hRadioWSnoTrailing, $iWSnoTrailing = 1 ? "Check" : "UnCheck", "")
	$hRadioWSOnlyInner = GUICtrlCreateRadio("Игнор начальных и закрывающих", 47, 444, 237, 20, -1, -1)
	GUICtrlSetState(-1, BitOR($GUI_UNCHECKED, $GUI_SHOW, $GUI_ENABLE))
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Не учитывать пробельные символы в начале и в конце строк")
	ControlCommand($hGUI, "", $hRadioWSOnlyInner, $iWSOnlyInner = 1 ? "Check" : "UnCheck", "")
	$hBtnClear1 = GUICtrlCreateButton("X", 281, 15, 36, 30, -1, -1)
	GUICtrlSetOnEvent(-1, "GUIclearEdit1")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Очистить список 1")
	$hBtnClear2 = GUICtrlCreateButton("X", 561, 15, 34, 30, -1, -1)
	GUICtrlSetOnEvent(-1, "GUIclearEdit2")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	GUICtrlSetTip(-1, "Очистить список 2")
	$hDummySelectAll = GUICtrlCreateDummy()
	GUICtrlSetOnEvent(-1, "GUIselectAllTextInEdit")
	Local $aAccelKeys[1][2] = [["^a", $hDummySelectAll]]
	GUISetAccelerators($aAccelKeys, $hGUI)
	$hProgbar = GUICtrlCreateProgress(47, 293, 548, 25, $PBS_MARQUEE, -1)
	GUICtrlSetState(-1, BitOR($GUI_HIDE, $GUI_ENABLE))
	GUISetState(@SW_SHOW, $hGUI)

	$g_oGUImain = ObjCreate("Scripting.Dictionary")
	$g_oGUImain.Add("$hGUI", $hGUI)
	$g_oGUImain.Add("$hBtnStart", $hBtnStart)
	$g_oGUImain.Add("$hFieldList1", $hFieldList1)
	$g_oGUImain.Add("$hFieldList2", $hFieldList2)
	$g_oGUImain.Add("$hBtnUnique1", $hBtnUnique1)
	$g_oGUImain.Add("$hBtnMatches", $hBtnMatches)
	$g_oGUImain.Add("$hBtnUnique2", $hBtnUnique2)
	$g_oGUImain.Add("$hRadioWSall", $hRadioWSall)
	$g_oGUImain.Add("$hRadioWSnoTrailing", $hRadioWSnoTrailing)
	$g_oGUImain.Add("$hRadioWSOnlyInner", $hRadioWSOnlyInner)
	$g_oGUImain.Add("$hBtnClear1", $hBtnClear1)
	$g_oGUImain.Add("$hBtnClear2", $hBtnClear2)
	$g_oGUImain.Add("$hDummySelectAll", $hDummySelectAll)
	$g_oGUImain.Add("$hProgbar", $hProgbar)
EndFunc   ;==>GUImainInit


Func GUIresInit()

	Local $hGUI
	Local $hField
	Local $hBtnCopy

	$hGUI = GUICreate($g_sWinTitle & ' - результаты', 362, 377, -1, -1, -1, -1, $g_oGUImain.Item("$hGUI"))
	GUISetOnEvent($GUI_EVENT_CLOSE, "GUIresClose", $hGUI)
	$hField = GUICtrlCreateEdit("", 30, 70, 303, 275, BitOR($ES_READONLY, $WS_VSCROLL, $WS_HSCROLL), -1)
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")
	$hBtnCopy = GUICtrlCreateButton("Скопировать в буфер", 100, 20, 158, 30, -1, -1)
	GUICtrlSetOnEvent(-1, "GUIresCopyData")
	GUICtrlSetFont(-1, 10, 400, 0, "MS Sans Serif")

	$g_oGUIres = ObjCreate("Scripting.Dictionary")
	$g_oGUIres.Add("$hGUI", $hGUI)
	$g_oGUIres.Add("$hField", $hField)
	$g_oGUIres.Add("$hBtnCopy", $hBtnCopy)
	$g_oGUIres.Add("$hBtnText", GUICtrlRead($g_oGUIres.Item("$hBtnCopy")))
EndFunc   ;==>GUIresInit


; простой GUI
While 1
	Sleep(50)
WEnd

#EndRegion ; инициализация


#Region ; базовые события интерфейсов

; очистка поля edit
Func GUIclearEdit($hField)
	GUICtrlSetData($hField, "")
EndFunc   ;==>GUIclearEdit


; получить данные из поля edit
Func GUIgetEditData($hControl, ByRef $aEditList)

	Local $sList = GUICtrlRead($hControl)

	; счётчик нельзя ставить, чтобы не отсвечивал в отчётах о сравнении
	$aEditList = StringSplit($sList, @CRLF, $STR_ENTIRESPLIT + $STR_NOCOUNT)

	; если поле edit не заполнено; помогает только в явных случаях,
	; если пустота распространяется на несколько строк, проще забить,
	; чем выявлять все ли строки пустые
	If (UBound($aEditList) = 1) And (Not StringStripWS($aEditList[0], 8)) Then

		MsgBox($MB_ICONWARNING, $g_sWinTitle, "Один из списков пуст, нечего сравнивать", 4, $g_oGUImain.Item("$hGUI"))
		Return SetError(1)
	EndIf
EndFunc   ;==>GUIgetEditData


Func GUICtrlChkboxRadRead($hControl)
	Return GUICtrlRead($hControl) = 1 ? 1 : 0
EndFunc   ;==>GUICtrlChkboxRadRead


Func GUImainSetBusy()

	GUICtrlSetState($g_oGUImain.Item("$hRadioWSall"), $GUI_DISABLE)
	GUICtrlSetState($g_oGUImain.Item("$hRadioWSnoTrailing"), $GUI_DISABLE)
	GUICtrlSetState($g_oGUImain.Item("$hRadioWSOnlyInner"), $GUI_DISABLE)
	GUICtrlSetState($g_oGUImain.Item("$hFieldList1"), $GUI_DISABLE)
	GUICtrlSetState($g_oGUImain.Item("$hFieldList2"), $GUI_DISABLE)
	GUICtrlSetState($g_oGUImain.Item("$hBtnClear1"), $GUI_DISABLE)
	GUICtrlSetState($g_oGUImain.Item("$hBtnClear2"), $GUI_DISABLE)
	GUICtrlSetState($g_oGUImain.Item("$hBtnStart"), $GUI_DISABLE)

	GUICtrlSetState($g_oGUImain.Item("$hBtnUnique1"), $GUI_HIDE)
	GUICtrlSetState($g_oGUImain.Item("$hBtnUnique2"), $GUI_HIDE)
	GUICtrlSetState($g_oGUImain.Item("$hBtnMatches"), $GUI_HIDE)

	GUICtrlSetState($g_oGUImain.Item("$hProgbar"), $GUI_SHOW)
	GUICtrlSendMsg($g_oGUImain.Item("$hProgbar"), $PBM_SETMARQUEE, 1, 50)
EndFunc   ;==>GUImainSetBusy


Func GUImainSetReady()

	GUICtrlSetState($g_oGUImain.Item("$hRadioWSall"), $GUI_ENABLE)
	GUICtrlSetState($g_oGUImain.Item("$hRadioWSnoTrailing"), $GUI_ENABLE)
	GUICtrlSetState($g_oGUImain.Item("$hRadioWSOnlyInner"), $GUI_ENABLE)
	GUICtrlSetState($g_oGUImain.Item("$hFieldList1"), $GUI_ENABLE)
	GUICtrlSetState($g_oGUImain.Item("$hFieldList2"), $GUI_ENABLE)
	GUICtrlSetState($g_oGUImain.Item("$hBtnClear1"), $GUI_ENABLE)
	GUICtrlSetState($g_oGUImain.Item("$hBtnClear2"), $GUI_ENABLE)
	GUICtrlSetState($g_oGUImain.Item("$hBtnStart"), $GUI_ENABLE)

	GUIsetResBtnState($g_oGUImain.Item("$hBtnUnique1"), $g_sResUnique1)
	GUIsetResBtnState($g_oGUImain.Item("$hBtnUnique2"), $g_sResUnique2)
	GUIsetResBtnState($g_oGUImain.Item("$hBtnMatches"), $g_sResMathes)

	GUICtrlSetState($g_oGUImain.Item("$hProgbar"), $GUI_HIDE)
	GUICtrlSendMsg($g_oGUImain.Item("$hProgbar"), $PBM_SETMARQUEE, 0, 50)

EndFunc   ;==>GUImainSetReady


; если по типу данных нет результатов, сделать кнопку неактивной
Func GUIsetResBtnState($hControl, ByRef $g_sString)

	If $g_sString Then
		GUICtrlSetState($hControl, BitOR($GUI_ENABLE, $GUI_SHOW))
	Else
		GUICtrlSetState($hControl, BitOR($GUI_DISABLE, $GUI_SHOW))
	EndIf
EndFunc   ;==>GUIsetResBtnState


; показ запрошенных данных в окне результатов
Func GUIresShow($hControl)

	Switch $hControl
		Case $g_oGUImain.Item("$hBtnUnique1")
			$g_sResCurrent = $g_sResUnique1

		Case $g_oGUImain.Item("$hBtnUnique2")
			$g_sResCurrent = $g_sResUnique2

		Case $g_oGUImain.Item("$hBtnMatches")
			$g_sResCurrent = $g_sResMathes
	EndSwitch

	GUICtrlSetData($g_oGUIres.Item("$hField"), $g_sResCurrent)
	GUISetState(@SW_SHOW, $g_oGUIres.Item("$hGUI"))
	GUISetState(@SW_DISABLE, $g_oGUImain.Item("$hGUI"))
EndFunc   ;==>GUIresShow


; вернуть кнопку копирования в интерфейсе результатов в исходное состояние
Func GUIresResetBtn()
	; снимаем регистрацию данной функции, если она есть
	AdlibUnRegister()
	GUICtrlSetData($g_oGUIres.Item("$hBtnCopy"), $g_oGUIres.Item("$hBtnText"))
	GUICtrlSetState($g_oGUIres.Item("$hBtnCopy"), $GUI_ENABLE)
EndFunc   ;==>GUIresResetBtn

#EndRegion ; базовые события интерфейсов


#Region ; события интерфейсов

Func _Exit()
	Exit
EndFunc   ;==>_Exit


; will make select all text in eny focused edit, as long as it is edit inside
; $hWin winhandle because we bound GUISetAccelerators to $hWin
Func GUIselectAllTextInEdit()

	Local $hWin = _WinAPI_GetFocus()
	Local $sClass = _WinAPI_GetClassName($hWin)
	If $sClass = 'Edit' Then _GUICtrlEdit_SetSel($hWin, 0, -1)

EndFunc   ;==>GUIselectAllTextInEdit


Func GUIclearEdit1()
	GUIclearEdit($g_oGUImain.Item("$hFieldList1"))
EndFunc   ;==>GUIclearEdit1

Func GUIclearEdit2()
	GUIclearEdit($g_oGUImain.Item("$hFieldList2"))
EndFunc   ;==>GUIclearEdit2


Func startMain()

	Local $aList1, $aList2
	GUIgetEditData($g_oGUImain.Item("$hFieldList1"), $aList1)
	If @error Then Return
	GUIgetEditData($g_oGUImain.Item("$hFieldList2"), $aList2)
	If @error Then Return

	GUImainSetBusy()
	setWSmode()
	main($aList1, $aList2)
	GUImainSetReady()

EndFunc   ;==>startMain


Func GUIresUnique1Show()
	GUIresShow($g_oGUImain.Item("$hBtnUnique1"))
EndFunc   ;==>GUIresUnique1Show

Func GUIresUnique2Show()
	GUIresShow($g_oGUImain.Item("$hBtnUnique2"))
EndFunc   ;==>GUIresUnique2Show

Func GUIresMatchesShow()
	GUIresShow($g_oGUImain.Item("$hBtnMatches"))
EndFunc   ;==>GUIresMatchesShow


; скопировать данные в буфер по нажатию кнопки
Func GUIresCopyData()

	ClipPut($g_sResCurrent)

	; кратковременно выключить кнопку и изменить её текст
	GUICtrlSetState($g_oGUIres.Item("$hBtnCopy"), $GUI_DISABLE)
	GUICtrlSetData($g_oGUIres.Item("$hBtnCopy"), "Скопировано")
	AdlibRegister("GUIresResetBtn", 5000)

EndFunc   ;==>GUIresCopyData


; по закрытия окна результатов
Func GUIresClose()
	; последовательность изменения состояния интерфейсов важна
	GUISetState(@SW_ENABLE, $g_oGUImain.Item("$hGUI"))
	GUISetState(@SW_HIDE, $g_oGUIres.Item("$hGUI"))
	GUIresResetBtn()
EndFunc   ;==>GUIresClose

#EndRegion ; события интерфейсов


#Region ядро ------------

Func main(ByRef $aList1, ByRef $aList2)

	Local $aUniq1, $aUniq2, $aMatched
	_ArraysCompare(0, $aList1, $aList2, $aUniq1, $aUniq2, $aMatched, $g_iWSmode - 1)

	; освобождаем память
	$aList1 = 0
	$aList2 = 0

	convertArrToStr($aUniq1, $g_sResUnique1)
	convertArrToStr($aUniq2, $g_sResUnique2)
	convertArrToStr($aMatched, $g_sResMathes)

EndFunc   ;==>main


; конвертация данных из массивов результатов;
; глобальная переменная задаётся как байреф, чтобы
; можно было использовать с несколькими глобальным переменными,
; а также, избежать передачи больших кусков данных между ф-ми
Func convertArrToStr(ByRef $aArray, ByRef $g_sString)

	; нет данных
	If UBound($aArray) = 0 Then

		$g_sString = Null
		Return SetError(1)
	EndIf

	$g_sString = _ArrayToString($aArray, @CRLF)
	If @error Then Return SetError(2)

EndFunc   ;==>convertArrToStr

#EndRegion ядро ------------


#Region ; вспомогательное

; установить и сохранить режим обработки пробелов
Func setWSmode()

	Local $iWSnoTrailing = GUICtrlChkboxRadRead($g_oGUImain.Item("$hRadioWSnoTrailing"))
	Local $iWSOnlyInner = GUICtrlChkboxRadRead($g_oGUImain.Item("$hRadioWSOnlyInner"))

	Local $iLastWSmode = $g_iWSmode
	Select
		Case $iWSOnlyInner
			$g_iWSmode = 3
		Case $iWSnoTrailing
			$g_iWSmode = 2
		Case Else
			$g_iWSmode = 1
	EndSelect

	If $g_iWSmode = $iLastWSmode Then Return
	IniWrite($g_sConfigFilePath, $g_sConfigSection, $g_sKeyWSmode, $g_iWSmode)

EndFunc   ;==>setWSmode

#EndRegion ; вспомогательное
